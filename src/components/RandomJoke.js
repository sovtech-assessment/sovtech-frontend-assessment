import React ,{ useEffect, createRef , useContext  }  from 'react';
import { gql, useLazyQuery } from '@apollo/client';
import Context from '../context/context';
import styled from 'styled-components';

const InnerContainer = styled.div `
    align-items : "center";
    align-content : "center";
    justify-content : "space-around";
    text-align : "center";
`

const Button = styled.button `
    padding : 10px;
    font-weight:600px;
    color : #ffffff;
    border-radius : 10px;
    /* background-color : ${props => (props.success ? "green" : "#d9534f")}; */
    background-color : blue;
    font-size: 15px;
    border : none;
    margin-top:20px;
`
const Input = styled.input `
    padding : 10px;
    border-radius: 10px;
    color : #000000;
`
export const GET_JOKE = gql`
    query getJokes($category: String!) {
        getRandomJokes(category:$category) {
        joke
            }
        }
    `;

const RandomJoke = () => {

const [getJoke, { loading, data, error  }] = useLazyQuery(GET_JOKE , {suspend : true});

const context = useContext(Context);

let jokeInput = createRef();

useEffect(() => {

    if (jokeInput.current !== null) {
        jokeInput.current.value = context.selectedCategory;
    } else {
        context.onCategorySelection("");
    }
},[context,jokeInput])


 if (loading) return(<div>loading..</div>)

 if (error) {
     context.createJoke(null);
 }

 if (data && data.getRandomJokes) {
     context.createJoke(data.getRandomJokes.joke);
 }

 const getJokeRequest = () => {

    if (jokeInput.current.value === null || jokeInput.current.value  === "") {
        alert("Please enter a category or select a category...");
        return;
    }

    getJoke({  variables : { category : jokeInput.current.value } });
 }

return (
    <InnerContainer>
        <h3>Select or type in a category </h3>
        <div>
            <Input ref={jokeInput} onChange={()=> context.selectedCategory} />
        </div>
        <div>
            <Button onClick={() => {getJokeRequest()}}> Search </Button>
        </div>
        {error && (<p> Something went wrong please try again...</p>)}
    </InnerContainer>
  );
}

export default RandomJoke;
