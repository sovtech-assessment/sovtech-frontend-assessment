import React ,  
       { useEffect, useContext }  from 'react';
import { useLazyQuery, gql  } from '@apollo/client';
import styled from 'styled-components';
import Context from '../context/context';

const CategoryTitle = styled.h1`
    margin-bottom : 20px;
`;

export const GET_CATEGORIES = gql`
    query getCategoryList {
      getCategoryList
    }
`

const CategoryList = ()  => {

const [getCategories, { loading, data, error  }] = useLazyQuery(GET_CATEGORIES);

const context = useContext(Context);

useEffect(() => {
  
},[data])

useEffect(() => {
    getCategories();
},[ getCategories])

if (loading) return(<div>loading..</div>)

if (error) {
return (<div>error</div>)
}

return (
    <div>
        <CategoryTitle>Category List :</CategoryTitle>
        {data && data.getCategoryList && (
            <div>
                {data.getCategoryList.map((category,index) => {
                    return ( <p onClick={()=> {context.onCategorySelection(category)}} key={index}>{category}</p>)
                })}
            </div>
        )}
    </div>
  );
}

export default CategoryList;
