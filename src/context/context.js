import React from 'react';

export default React.createContext({
    categories : null,
    getCategories : () => {},
    createJoke : () => {},
    createdJoke : null,
    selectedCategory : null,
    onCategorySelection : (category) => {}
});