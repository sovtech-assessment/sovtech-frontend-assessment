import React ,  { useState, Suspense  }  from 'react';
import './App.css';

import Home from './Pages/Home';

import Context from './context/context';

function App() {

  const [createdJoke , setCreatedJoke] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState("");

  const createJoke = (joke) => {
    setCreatedJoke(joke);
  };

  const onCategorySelection = (category) => {
    setSelectedCategory(category)
  }

  return (
    <Context.Provider 
    value={{ 
      createJoke : createJoke,
      createdJoke : createdJoke,
      onCategorySelection : onCategorySelection,
      selectedCategory : selectedCategory
     }}
    >
      <Suspense fallback={<div>loading..suspense</div>}>
        <Home/>
      </Suspense>
    </Context.Provider>
  );
}

export default App;
