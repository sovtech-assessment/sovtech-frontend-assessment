import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { ApolloClient, InMemoryCache, ApolloProvider, ApolloLink, HttpLink, concat } from '@apollo/client';
console.log(process.env)
const httpLink = new HttpLink({ uri: process.env.REACT_APP_STAGING_API});

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: "Bearer imaginaryToken==134124bjkb4124bjkb412jk4bjk12b4k21b4kjb421j4bk1b2j41k2j4b1k2b4kj1241", // create dummy token;
    }
  });
  return forward(operation);
})

const client = new ApolloClient({
  link : concat(authMiddleware,httpLink),
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
