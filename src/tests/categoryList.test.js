import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import renderer from 'react-test-renderer';
import CategoryList,{ GET_CATEGORIES } from '../components/CategoryList';
import {GraphQLError} from 'graphql';
import { getByText } from '@testing-library/react';

async function wait(ms = 0) {
    await renderer.act(() => {
      return new Promise(resolve => {
        setTimeout(resolve, ms);
      });
    });
  }

const mocks = [
    {
        request: {
            query : GET_CATEGORIES,
            variables : {},
        },
        result : {
            data : {
                categories : ["animal","career"]
            },
        },
    },
]
describe('Get a list of Categories', () => {
it('renders without error', () => {
        const component = renderer.create(
            <MockedProvider mocks={mocks} addTypename={false}>
                <CategoryList categories={["animal","career"]}/>
            </MockedProvider>,
        )
    })
    it('shows error',() => {
        const mockedError = {
          request: {
            query: GET_CATEGORIES,
          },
          // error: new Error('Oops we can not get the products'),
          result: {
            errors: [new GraphQLError('Error Getting Categories.. Please try again later')],
          },
        };
        const component = renderer.create(
          <MockedProvider mocks={[mockedError]} addTypename={false}>
            <CategoryList />
          </MockedProvider>,
        );
        })
});
