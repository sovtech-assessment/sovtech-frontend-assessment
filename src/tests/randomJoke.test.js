import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import renderer from 'react-test-renderer';
import RandomJoke , { GET_JOKE }  from '../components/RandomJoke';

const mocks = [
    {
        request: {
            query : GET_JOKE,
            variables : { category : "animal" },
        },
        result : {
            data : {
                joke : { value : "Chuck Norris does not follow fashion trends, they …and kicks their ass. Nobody follows Chuck Norris." },
            },
        },
    },
]
describe('RandomJoke', () => {
it('renders without error', () => {
        const component = renderer.create(
            <MockedProvider mocks={mocks} addTypename={false}>
                <RandomJoke joke="Chuck Norris does not follow fashion trends, they …and kicks their ass. Nobody follows Chuck Norris."/>
            </MockedProvider>,
        )
    })
})
