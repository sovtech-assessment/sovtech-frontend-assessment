import React, { useContext } from 'react';
import styled from 'styled-components';

import CategoryList from '../components/CategoryList';
import RandomJoke from '../components/RandomJoke';
import Context from '../context/context';

const Container = styled.div `
    max-width: 960px;
    margin : 0 auto;
    padding : 20px;
    text-align : center;
`
const Header = styled.h1 `
    font-family: 'Roboto',sans-serif;
    margin : 20px 0px;
    text-align : center;
`

const Paragraph = styled.p `
    text-align : center;
    margin: 20px 0px;
`

const Home = ()  => {

const context = useContext(Context);

return (
    <Container>
     <Header>{context.createdJoke === null ? "Let's generate a new joke" : "New joke generated"}</Header>
     <Paragraph>{context.createdJoke}</Paragraph>
     <RandomJoke />
     <CategoryList />
    </Container>
  );
}

export default Home;
